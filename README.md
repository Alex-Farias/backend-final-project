# Backend Final Project

## Vis�o Geral

Este � um projeto backend desenvolvido com Spring Boot. O projeto gerencia cursos, matr�culas e estudantes. Ele utiliza JPA/Hibernate para persist�ncia de dados e um banco de dados H2 para armazenamento.

## Requisitos

Antes de iniciar a instala��o, verifique se voc� possui os seguintes requisitos atendidos:

- **Java Development Kit (JDK) 17+**

  - Voc� pode baixar o JDK [aqui](https://www.oracle.com/java/technologies/javase-jdk17-downloads.html).

- **Maven 3.6+**

  - Voc� pode baixar o Maven [aqui](https://maven.apache.org/download.cgi).

- **Docker Desktop**
  - Voc� pode baixar o Docker Desktop [aqui](https://www.docker.com/products/docker-desktop/).

## Instala��o

1. **Clone o reposit�rio**

- Para clonar o reposit�rio git com c�digo, cole os seguintes comando no terminal bash do local que deseja realizar o clone
  git clone https://github.com/seu-usuario/backend_final_project.git
  cd backend_final_project

## Endpoints

Abaixo est�o listados os principais endpoints dispon�veis na API:

### Cursos

- GET /curso: Obt�m uma lista paginada de todos os cursos.
- GET /curso/{id}: Obt�m um curso espec�fico por ID.
- POST /curso: Cria um novo curso.
- PUT /curso/{id}: Atualiza um curso existente por ID.
- DELETE /curso/{id}: Deleta um curso espec�fico por ID.

### Estudantes

- GET /aluno: Obt�m uma lista paginada de todos os estudantes.
- GET /aluno/{id}: Obt�m um estudante espec�fico por ID.
- POST /aluno: Cria um novo estudante.
- PUT /aluno/{id}: Atualiza um estudante existente por ID.
- DELETE /aluno/{id}: Deleta um estudante espec�fico por ID.

### Matr�culas

- GET /matricula: Obt�m uma lista de todas as matr�culas.
- GET /matricula/{id}: Obt�m uma matr�cula espec�fica por ID.
- POST /matricula: Cria uma nova matr�cula.
- PUT /matricula/{id}: Atualiza uma matr�cula existente por ID.
- DELETE /matricula/{id}: Deleta uma matr�cula espec�fica por ID.

## Acesso ao Console do H2

O console do H2 Database est� dispon�vel em http://localhost:8080/h2-console. Use as credenciais configuradas no arquivo application.properties para acessar o console.

- JDBC URL: jdbc:h2:mem:testdb
- User Name: sa
- Password: password

## Contribui��es

- Alex Farias De Abreu Nabo - @Alex-Farias<br>
- Matheus Araildi - @Araldi42<br>
