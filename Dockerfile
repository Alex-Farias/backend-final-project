# Use a imagem base do OpenJDK para Java 17
FROM openjdk:17-jdk-alpine

# Define o diret�rio de trabalho dentro do cont�iner
WORKDIR /app

# Copie o arquivo JAR do seu aplicativo para o diret�rio de trabalho no cont�iner
COPY target/backend_final_project.jar /app/backend_final_project.jar

# Comando para executar o aplicativo quando o cont�iner for iniciado
CMD ["java", "-jar", "backend_final_project.jar"]
