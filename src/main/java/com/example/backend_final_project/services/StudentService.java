package com.example.backend_final_project.services;

import com.example.backend_final_project.models.Student;
import com.example.backend_final_project.repositories.StudentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class StudentService {
    private final StudentRepository studentRepository;

    @Autowired
    public StudentService(StudentRepository studentRepository) {
        this.studentRepository = studentRepository;
    }

    public Page<Student> getAll(Pageable pageable) {
        return studentRepository.findAll(pageable);
    }

    public Optional<Student> getById(Long id) {
        return studentRepository.findById(id);
    }

    public Student save(Student aluno) {
        return studentRepository.save(aluno);
    }

    public void deleteById(Long id) {
        studentRepository.deleteById(id);
    }
}
