package com.example.backend_final_project.services;

import com.example.backend_final_project.models.Course;
import com.example.backend_final_project.repositories.CourseRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class CourseService {
    private final CourseRepository courseRepository;

    @Autowired
    public CourseService(CourseRepository courseRepository) {
        this.courseRepository = courseRepository;
    }

    public Page<Course> getAll(Pageable pageable){
        return courseRepository.findAll(pageable);
    }

    public Optional<Course> getById(Long id){
        return courseRepository.findById(id);
    }

    public Course save(Course course){
        return courseRepository.save(course);
    }

    public void deleteById(Long id){
        courseRepository.deleteById(id);
    }
}
