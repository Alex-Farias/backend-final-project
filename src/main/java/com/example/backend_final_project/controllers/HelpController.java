package com.example.backend_final_project.controllers;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/ajuda")
public class HelpController {

    @GetMapping
    public String getHelpContent() {
        String students = "[\"Alex Farias\", \"Mateus Araldi\"]";
        String project = "Plataforma de Cursos Livres";
        String theme = "Acesso à Educação";

        return "{\n" +
                "  \"estudantes\": " + students + ",\n" +
                "  \"projeto\": \"" + project + "\",\n" +
                "  \"tema\": \"" + theme + "\"\n" +
                "}";
    }
}
