package com.example.backend_final_project.controllers;

import com.example.backend_final_project.models.Registry;
import com.example.backend_final_project.repositories.RegistryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@RequestMapping("/matricula")
public class RegistryController {

    private final RegistryRepository registryRepository;

    @Autowired
    public RegistryController(RegistryRepository registryRepository) {
        this.registryRepository = registryRepository;
    }

    @GetMapping
    public ResponseEntity<Iterable<Registry>> getAllRegistries() {
        Iterable<Registry> registries = registryRepository.findAll();
        return ResponseEntity.ok(registries);
    }

    @GetMapping("/{id}")
    public ResponseEntity<Registry> getRegistryById(@PathVariable Long id) {
        Optional<Registry> registry = registryRepository.findById(id);
        return registry.map(ResponseEntity::ok)
                .orElseGet(() -> ResponseEntity.notFound().build());
    }

    @PostMapping
    public ResponseEntity<Registry> createRegistry(@RequestBody Registry registry) {
        Registry savedRegistry = registryRepository.save(registry);
        return ResponseEntity.status(HttpStatus.CREATED).body(savedRegistry);
    }

    @PutMapping("/{id}")
    public ResponseEntity<Registry> updateRegistry(@PathVariable Long id, @RequestBody Registry registry) {
        Optional<Registry> existingRegistry = registryRepository.findById(id);
        if (existingRegistry.isPresent()) {
            registry.setId(id);
            Registry updatedRegistry = registryRepository.save(registry);
            return ResponseEntity.ok(updatedRegistry);
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteRegistry(@PathVariable Long id) {
        Optional<Registry> existingRegistry = registryRepository.findById(id);
        if (existingRegistry.isPresent()) {
            registryRepository.deleteById(id);
            return ResponseEntity.noContent().build();
        } else {
            return ResponseEntity.notFound().build();
        }
    }
}
