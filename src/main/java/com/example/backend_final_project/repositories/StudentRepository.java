package com.example.backend_final_project.repositories;

import com.example.backend_final_project.models.Student;
import org.springframework.data.jpa.repository.JpaRepository;

public interface StudentRepository extends JpaRepository<Student, Long> {
}
