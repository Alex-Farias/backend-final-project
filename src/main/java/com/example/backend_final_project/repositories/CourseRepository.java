package com.example.backend_final_project.repositories;

import com.example.backend_final_project.models.Course;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CourseRepository extends JpaRepository<Course, Long> {
}
