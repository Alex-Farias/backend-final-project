package com.example.backend_final_project.repositories;

import com.example.backend_final_project.models.Registry;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

public interface RegistryRepository extends JpaRepository<Registry, Long> {
}
